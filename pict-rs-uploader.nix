{ lib
, nixosTests
, rustPlatform
}:

rustPlatform.buildRustPackage {
  pname = "pict-rs-uploader";
  version = "0.1.0";
  src = ./.;
  cargoLock.lockFile = ./Cargo.lock;

  nativeBuildInputs = [ ];

  passthru.tests = { inherit (nixosTests) pict-rs-uploader; };

  meta = with lib; {
    description = "A simple image hosting service";
    homepage = "https://git.asonix.dog/asonix/pict-rs-uploader";
    license = with licenses; [ agpl3Plus ];
  };
}
